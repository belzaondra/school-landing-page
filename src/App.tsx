import React from "react";
import "./App.css";

interface Project {
  name: string;
  link: string;
}

function App() {
  const projects: Project[] = [
    {
      name: "Weather app",
      link: "https://weather-app.belzaondrej.com/",
    },
    {
      name: "Calculator",
      link: "https://calculator.belzaondrej.com/",
    },
    {
      name: "Password generator",
      link: "https://password-generator.belzaondrej.com/",
    },
  ];
  return (
    <>
      <div className="container mx-auto mt-4 md:mt-20">
        <h1 className="text-4xl md:text-5xl font-bold mx-auto text-center">
          Hello there
        </h1>
        <p className="text-4xl md:text-5xl mt-4 text-center">👋</p>
        <p className="text-center text-xl mt-4">
          Feel free to check out my school projects
        </p>

        <img
          alt="funny gif"
          className="mx-auto my-4 rounded"
          src="https://media.giphy.com/media/SSM6HdOicCahnOZ5hM/giphy.gif"
        />

        <ul className="text-center mx-auto mt-4">
          {projects.map((p) => (
            <li key={p.name} className="mt-2">
              {p.name}
              <a
                href={p.link}
                target="_blank"
                rel="noreferrer"
                className="text-blue-500 ml-4 hover:underline"
              >
                (Link)
              </a>
            </li>
          ))}
        </ul>
      </div>

      <p className="text-center my-8">
        © Ondrej Belza
        <a
          className="text-blue-500 ml-4 hover:underline"
          href="https://gitlab.com/belzaondra"
        >
          GitLab
        </a>
        <a
          className="text-blue-500 ml-4 hover:underline"
          href="https://www.linkedin.com/in/ond%C5%99ej-belza-b8484a187/"
        >
          LinkedIn
        </a>
      </p>
    </>
  );
}

export default App;
